package co.jp.collectcommunication.adapter

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView.OnItemClickListener
import android.widget.BaseAdapter
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import co.jp.collectcommunication.Attachment
import co.jp.collectcommunication.AttachmentType
import co.jp.collectcommunication.DisplayImageFragment
import co.jp.collectcommunication.R
import com.bumptech.glide.Glide
import com.connectycube.chat.model.ConnectycubeAttachment


/**
 * Created by Alex.Tran
 */
class AttachmentAdapter(
    var context: Context,
    var items: ArrayList<Attachment>
) :
    RecyclerView.Adapter<AttachmentAdapter.AttacmentViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AttacmentViewHolder {
        return if (viewType == 1) {
            TitleHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.item_attacment_title, parent, false)
            )
        } else {
            ItemHolder(
                LayoutInflater.from(context)
                    .inflate(R.layout.item_attacment, parent, false)
            )
        }
    }

    override fun getItemViewType(position: Int): Int =
        if (items[position].type == AttachmentType.TITLE) 1 else 0

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder:AttacmentViewHolder, position: Int) {
        holder.onBindView(items[position])
    }


    abstract class AttacmentViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {
            abstract fun onBindView(item: Attachment)
    }

    class TitleHolder(private val rootView: View) : AttacmentViewHolder(rootView) {
        override fun onBindView(item: Attachment) {
            rootView.findViewById<TextView>(R.id.titleDate).text = item.title
        }
    }

    class ItemHolder(val rootView: View) : AttacmentViewHolder(rootView) {
        override fun onBindView(item: Attachment) {
            Glide.with(rootView.context).load(item.items?.url)
                .placeholder(R.drawable.ic_image_black_24dp).centerCrop()
                .into(rootView.findViewById(R.id.image_view))

            rootView.findViewById<ImageView>(R.id.image_view).setOnClickListener {
                val manager = (rootView.context as AppCompatActivity).supportFragmentManager
                val transaction = manager.beginTransaction()
                transaction.setCustomAnimations(
                    android.R.anim.slide_in_left,
                    android.R.anim.slide_out_right
                )
                transaction.add(
                    R.id.framelayout,
                    DisplayImageFragment(item.items?.url),
                    DisplayImageFragment::class.java.canonicalName
                )
                transaction.addToBackStack(DisplayImageFragment::class.java.canonicalName)
                transaction.commit()
            }
        }
    }
}