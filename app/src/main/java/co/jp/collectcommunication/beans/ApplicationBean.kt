package co.jp.collectcommunication.beans

import com.google.gson.annotations.SerializedName


data class ApplicationBean(
    @SerializedName("login_appicon") val login_appicon: String?,
    @SerializedName("login_background_img") val login_background_img: String?,
    @SerializedName("login_title") val login_title: String?,
    //Server config
    @SerializedName("chat_id") val chat_id: Int,
    @SerializedName("chat_authkey") val chat_authkey: String?,
    @SerializedName("chat_authsecret") val chat_authsecret: String?,
    @SerializedName("chat_accountkey") val chat_accountkey: String?
) {
    @SerializedName("application_id")
    val application_id: Int = -1
    @SerializedName("application_name")
    val application_name: String = ""

    //server info
    @SerializedName("status")
    val status: Int = -1
    @SerializedName("created_at")
    val created_at: Int = -1
    @SerializedName("updated_at")
    val updated_at: Int = -1
}
