package co.jp.collectcommunication.beans

import com.google.gson.annotations.SerializedName

data class ChatRoomBean(
    @SerializedName("account_id") val account_id: String?,
    @SerializedName("main_top_img") val main_top_img: String?,
    @SerializedName("main_top_title") val main_top_title: String?,
    @SerializedName("main_menu_order") val main_menu_order: String?,
    @SerializedName("main_menu_shopping") val main_menu_shopping: String?,
    @SerializedName("main_menu_shopinfo") val main_menu_shopinfo: String?
) {
    //server info
    @SerializedName("status")
    val status: Int = -1
    @SerializedName("created_at")
    val created_at: Int = -1
    @SerializedName("updated_at")
    val updated_at: Int = -1
}