package co.jp.collectcommunication

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import co.jp.collectcommunication.beans.ApplicationBean
import co.jp.collectcommunication.pref.ApplicationPref
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
        NetWorkUtility.fcMessengerApps(object : VoidCallBack {
            override fun onComplete() {
                val bean = ApplicationPref.instance.getApplication(this@SplashActivity)
                Glide.with(this@SplashActivity).load(bean.login_appicon)
                    .diskCacheStrategy(
                        DiskCacheStrategy.ALL
                    ).skipMemoryCache(true).into(spl_logo)
                Glide.with(this@SplashActivity).load(bean.login_background_img)
                    .diskCacheStrategy(
                        DiskCacheStrategy.ALL
                    ).skipMemoryCache(true).into(spl_background)
                Glide.with(this@SplashActivity).load(bean.login_title)
                    .diskCacheStrategy(
                        DiskCacheStrategy.ALL
                    ).skipMemoryCache(true).into(spl_title)

                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }

            override fun onFailure() {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
        }, this@SplashActivity)
    }
}