package co.jp.collectcommunication

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import co.jp.collectcommunication.pref.SharedPreferencesManager
import android.util.DisplayMetrics
import android.view.WindowManager
import androidx.multidex.MultiDex
import co.jp.collectcommunication.pref.ApplicationPref

class App : Application() {

    companion object {
        var SCREEN_WIDTH = -1
        var SCREEN_HEIGHT = -1
        var DIMEN_RATE = -1.0f
        var DIMEN_DPI = -1
        private var instance: App? = null
        fun applicationContext() : App {
            return instance as App
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        MultiDex.install(this)

        val prefs: SharedPreferences = getSharedPreferences("com.mycompany.myAppName", MODE_PRIVATE)
        if (prefs.getBoolean("FIRST_RUN_${BuildConfig.VERSION_CODE}", true)) {
            SharedPreferencesManager.getInstance(this).deleteCurrentUser()
            ApplicationPref.instance.clear(this)
            prefs.edit()?.putBoolean("FIRST_RUN_${BuildConfig.VERSION_CODE}", false)?.apply()
        }

        getScreenSize()
    }

    private fun getScreenSize() {
        val windowManager: WindowManager? =
            this.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        val dm = DisplayMetrics()
        windowManager?.run {
            val display = defaultDisplay
            display.getMetrics(dm)
            DIMEN_RATE = dm.density / 1.0f
            DIMEN_DPI = dm.densityDpi
            SCREEN_WIDTH = dm.widthPixels
            SCREEN_HEIGHT = dm.heightPixels
            if (SCREEN_WIDTH > SCREEN_HEIGHT) {
                val t = SCREEN_HEIGHT
                SCREEN_HEIGHT = SCREEN_WIDTH
                SCREEN_WIDTH = t
            }
        }

    }
}