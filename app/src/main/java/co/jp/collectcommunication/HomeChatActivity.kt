package co.jp.collectcommunication

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.provider.Settings
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_chatmessages.*
import android.graphics.drawable.ColorDrawable
import android.view.Window.FEATURE_NO_TITLE
import android.app.Activity
import android.app.Dialog
import android.graphics.Typeface
import android.os.StrictMode
import android.provider.MediaStore
import android.text.format.DateFormat
import android.util.Log
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import co.jp.collectcommunication.pref.ApplicationPref
import co.jp.collectcommunication.pref.SharedPreferencesManager
import co.jp.collectcommunication.util.UploadAttachment
import co.jp.collectcommunication.util.loadAvatarPhoto
import com.bumptech.glide.Glide
import com.connectycube.auth.session.ConnectycubeSessionManager
import com.connectycube.core.EntityCallback
import com.connectycube.core.exception.ResponseException
import com.connectycube.storage.ConnectycubeStorage
import com.connectycube.storage.model.ConnectycubeFile
import com.connectycube.users.ConnectycubeUsers
import com.connectycube.users.model.ConnectycubeUser
import kotlinx.android.synthetic.main.activity_chatmessages.loadingProgressBar
import kotlinx.android.synthetic.main.activity_chatmessages.version
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

const val CHANGE_AVATAR_GALLERY = 1992
const val CHANGE_AVATAR_CAMERA = 1991
const val REUQEST_SETTING = 101

/**
 * Created by Alex.Tran
 */
class HomeChatActivity : AppCompatActivity() {
    private var mCurrentFilterValue: Int = R.id.menu_filter_popular
    private lateinit var homeChatViewModel: HomeChatViewModel
    var mCameraFileName = ""
    var dialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chatmessages)
        val bean = ApplicationPref.instance.getChatRoom(this)
        Glide.with(this)
            .asDrawable()
            .load(bean.main_top_img)
            .into(background_header)

        chat_title.text = bean.main_top_title
        val typeface = Typeface.createFromAsset(assets, "HGRMB.TTC")
        chat_title.typeface = typeface

        val pInfo = packageManager.getPackageInfo(packageName, 0)
        version.text = "ver ${pInfo.versionName}"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent()
            val packageName = packageName
            val pm = getSystemService(POWER_SERVICE) as PowerManager
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                intent.data = Uri.parse("package:$packageName")
                startActivity(intent)
            }
        }

        homeChatViewModel = ViewModelProviders.of(this).get(HomeChatViewModel::class.java)

        val user = SharedPreferencesManager.getInstance(this@HomeChatActivity).getCurrentUser()
        homeChatViewModel.shopInfo = user
        homeChatViewModel.getUserShopInfo(context = this@HomeChatActivity)

        imageMenu.setOnClickListener {
            imageMenu.visibility = View.INVISIBLE
            imageMenuClose.visibility = View.VISIBLE
            getFragmentChat()?.hideInputMethod()
            showPopup(it, this@HomeChatActivity)
        }

        val manager = supportFragmentManager
        val transaction = manager.beginTransaction()

        transaction.add(R.id.framelayout, ChatFragment(), ChatFragment::class.java.canonicalName)
        transaction.addToBackStack(ChatFragment::class.java.canonicalName)
        transaction.commit()
    }

    override fun onBackPressed() {
        val topFragment: Fragment?
        val manager = supportFragmentManager
        val count = manager.backStackEntryCount
        if (count > 0) {
            val tag = manager.getBackStackEntryAt(count - 1).name
            topFragment = manager.findFragmentByTag(tag)
            if (topFragment !is ChatFragment)
                super.onBackPressed()
            try {
                if (manager.findFragmentById(R.id.framelayout) is ChatFragment) {
                    imageMenu.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
            }
        }


    }

    private fun showPopup(v: View, activity: Context) {
        val popup =
            PopupMenu(activity, v, Gravity.START or Gravity.BOTTOM, 0, R.style.FilterPopupMenuStyle)
        try {
            // Reflection apis to enforce show icon
            val fields = popup.javaClass.declaredFields
            for (i in fields.indices) {
                if (fields[i].name == POPUP_CONSTANT) {
                    fields[i].isAccessible = true
                    val menuPopupHelper = fields[i].get(popup)
                    val classPopupHelper = Class.forName(menuPopupHelper.javaClass.name)
                    val setForceIcons =
                        classPopupHelper.getMethod(
                            POPUP_FORCE_SHOW_ICON,
                            Boolean::class.javaPrimitiveType
                        )
                    setForceIcons.invoke(menuPopupHelper, true)
                    break
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        popup.menuInflater.inflate(R.menu.menu_sort, popup.menu)
//        popup.menu.findItem(mCurrentFilterValue).setIcon(R.drawable.ic_menu_check)
        popup.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                val bean = ApplicationPref.instance.getChatRoom(this@HomeChatActivity)

                when (item?.itemId) {
                    R.id.menu_filter_popular -> {
                        isNotDisconnectedRoomChat = true
                        imageMenu.visibility = View.INVISIBLE
                        filterList(R.id.menu_filter_popular)
                        val transaction = supportFragmentManager.beginTransaction()
                        transaction.add(
                            R.id.framelayout,
                            AttachmentFragment(),
                            AttachmentFragment::class.java.canonicalName
                        )
                        transaction.addToBackStack(AttachmentFragment::class.java.canonicalName)
                        transaction.commitAllowingStateLoss()
                        return true
                    }
                    R.id.menu_filter_lth -> {
                        filterList(R.id.menu_filter_lth)
                        bean.main_menu_order?.let { openUrl(it) }
                        isNotDisconnectedRoomChat = true
                        return true
                    }
                    R.id.menu_filter_htl -> {
                        filterList(R.id.menu_filter_htl)
                        bean.main_menu_shopping?.let { openUrl(it) }
                        isNotDisconnectedRoomChat = true
                        return true
                    }
                    R.id.menu_filter_nto -> {
                        filterList(R.id.menu_filter_nto)
                        bean.main_menu_shopinfo?.let { openUrl(it) }
                        isNotDisconnectedRoomChat = true
                        return true
                    }

                    R.id.menu_change_avatar -> {
                        filterList(R.id.menu_change_avatar)
                        showDialog(this@HomeChatActivity)
                        return true
                    }
                    R.id.menu_filter_otn -> {
                        filterList(R.id.menu_filter_otn)
                        isNotDisconnectedRoomChat = true
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri = Uri.fromParts("package", packageName, null)
                        intent.data = uri
                        startActivityForResult(intent, REUQEST_SETTING)
                        return true
                    }
                    else -> return false
                }
            }
        })
        popup.show()
        popup.setOnDismissListener {
            imageMenuClose.visibility = View.INVISIBLE
            imageMenu.visibility = View.VISIBLE
        }
    }

    fun showDialog(activity: Activity) {

        dialog?.dismiss()
        dialog = Dialog(activity)

        dialog?.run {
            requestWindowFeature(FEATURE_NO_TITLE)
            setCancelable(false)
            setContentView(R.layout.layout_dialog_change_avatar)
            window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

            val sessionParameters = ConnectycubeSessionManager.getInstance().sessionParameters
            val activeSession = ConnectycubeSessionManager.getInstance().activeSession
            val user = SharedPreferencesManager.getInstance(activity).getCurrentUser()

            findViewById<TextView>(R.id.user_id).text =
                String.format(resources.getString(R.string.title_id), sessionParameters.userId)
            findViewById<TextView>(R.id.user_name).text =
                String.format(resources.getString(R.string.title_user), user.fullName)
            findViewById<TextView>(R.id.user_last_login).text =
                String.format(
                    resources.getString(R.string.title_last_login),
                    getDate(activeSession.timestamp)
                )

            findViewById<ImageView>(R.id.avatar_picker).setOnClickListener {
                val pm = PopupMenu(this@HomeChatActivity, it)
                pm.menuInflater.inflate(R.menu.picker_media_menu, pm.menu)
                pm.setOnMenuItemClickListener { menuItem ->
                    if (menuItem.itemId == R.id.menu_take_photo) {
                        takePhotoFromCamera()
                    } else if (menuItem.itemId == R.id.menu_select_photo_from_device) {
                        choosePhotoFromGallery()
                    }
                    return@setOnMenuItemClickListener true
                }
                pm.show()
            }

            loadAvatarPhoto(user.avatar, findViewById(R.id.profile_image), activity)
            findViewById<TextView>(R.id.accept_change_avatar).setOnClickListener {
                dismiss()
            }
            show()
        }
    }

    private fun choosePhotoFromGallery() {
        isNotDisconnectedRoomChat = true
        startActivityForResult(
            Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            ), CHANGE_AVATAR_GALLERY
        )
    }

    private fun takePhotoFromCamera() {
        isNotDisconnectedRoomChat = true
        cameraIntent()
    }

    private fun cameraIntent() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE

        val newPicFile = "image" + SimpleDateFormat("-mm-ss").format(Date()) + ".jpg"
        val outPath = "/sdcard/$newPicFile"
        val outFile = File(outPath)

        mCameraFileName = outFile.toString()
        val outUri = Uri.fromFile(outFile)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outUri)
        startActivityForResult(intent, CHANGE_AVATAR_CAMERA)
    }

    private fun getDate(time: Long): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = time * 1000
        return DateFormat.format("yyyy年MM月dd日-HH時mm分", cal).toString()
    }


    fun openUrl(url: String) {
        try {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(url)
            startActivity(openURL)
        } catch (e: Exception) {
        }
    }

    fun filterList(index: Int) {
        mCurrentFilterValue = index
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CHANGE_AVATAR_GALLERY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val path = UploadAttachment.getPathFromURI(contentURI, this@HomeChatActivity)
                    if (path != null) {
                        dialog?.run {
                            val filePath = File(path)
                            changeAvatar(filePath)
                            dismiss()
                        }
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        } else if (requestCode == CHANGE_AVATAR_CAMERA && resultCode == Activity.RESULT_OK) {
            dialog?.run {
                val filePath = File(mCameraFileName)
                changeAvatar(filePath)
                dismiss()
            }
        } else if (requestCode == REUQEST_SETTING) {

        }
    }

    private fun changeAvatar(imageFile: File) {
        showProgress()
        progress.visibility = View.VISIBLE
        progress2.visibility = View.GONE
        progress.progress = 0

        ConnectycubeStorage.uploadFileTask(
            imageFile, true
        ) { p0 ->
            progress.progress = p0
        }
            .performAsync(object : EntityCallback<ConnectycubeFile> {
                override fun onSuccess(connectycubeFile: ConnectycubeFile?, p1: Bundle?) {
                    progress.visibility = View.GONE
                    progress2.visibility = View.VISIBLE

                    connectycubeFile?.let {
                        val sessionParameters =
                            ConnectycubeSessionManager.getInstance().sessionParameters
                        val user = ConnectycubeUser(sessionParameters.userId)
                        user.avatar = it.publicUrl
                        ConnectycubeUsers.updateUser(user)
                            .performAsync(object : EntityCallback<ConnectycubeUser> {
                                override fun onSuccess(user: ConnectycubeUser, args: Bundle) {
                                    val oldUser =
                                        SharedPreferencesManager.getInstance(this@HomeChatActivity)
                                            .getCurrentUser()
                                    user.password = ConnectycubeSessionManager.getInstance()
                                        .sessionParameters.userPassword
                                    user.customData = oldUser.customData
                                    SharedPreferencesManager.getInstance(applicationContext)
                                        .saveCurrentUser(user)
                                    getFragmentChat()?.updateFragmentData()
                                    hideProgress()
                                }

                                override fun onError(error: ResponseException) {
                                    hideProgress()
                                    Toast.makeText(
                                        applicationContext,
                                        getString(R.string.login_chat_login_error),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            })
                    }

                }

                override fun onError(p0: ResponseException?) {
                    hideProgress()
                }

            })
    }

    private fun hideProgress() {
        window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        loadingProgressBar.visibility = View.GONE
    }

    private fun showProgress() {
        window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        loadingProgressBar.visibility = View.VISIBLE
    }

    private fun getFragmentChat(): ChatFragment? {
        return if (supportFragmentManager.findFragmentById(R.id.framelayout) is ChatFragment)
            supportFragmentManager.findFragmentById(R.id.framelayout) as ChatFragment else null

    }

    companion object {
        const val POPUP_FORCE_SHOW_ICON = "setForceShowIcon"
        const val POPUP_CONSTANT = "mPopup"
    }

//    private fun signOut() {
//        ConnectycubeChatService.getInstance().destroy()
//        ConnectycubeUsers.signOut().performAsync(object : EntityCallback<Void> {
//            override fun onSuccess(p0: Void?, p1: Bundle?) {
//                SharedPreferencesManager.getInstance(applicationContext).deleteCurrentUser()
//                startLoginActivity()
//                this@HomeChatActivity.finish()
//            }
//
//            override fun onError(p0: ResponseException?) {
//            }
//
//        })
//    }

//    private fun startLoginActivity() {
//        val intent = Intent(this@HomeChatActivity, LoginActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
//        this.startActivity(intent)
//    }
}