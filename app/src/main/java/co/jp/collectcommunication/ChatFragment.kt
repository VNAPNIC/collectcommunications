package co.jp.collectcommunication

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import co.jp.collectcommunication.pref.SharedPreferencesManager
import co.jp.collectcommunication.util.*
import co.jp.collectcommunication.viewmodel.ConnectycubeMessageSender
import com.connectycube.chat.ConnectycubeChatService
import com.connectycube.chat.exception.ChatException
import com.connectycube.chat.listeners.ChatDialogMessageListener
import com.connectycube.chat.listeners.ChatDialogTypingListener
import com.connectycube.chat.listeners.MessageStatusListener
import com.connectycube.chat.model.ConnectycubeAttachment
import com.connectycube.chat.model.ConnectycubeChatMessage
import com.connectycube.chat.model.ConnectycubeDialogType
import com.connectycube.core.EntityCallback
import com.connectycube.core.exception.ResponseException
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.internal.entity.CaptureStrategy
import id.zelory.compressor.Compressor
import kotlinx.android.synthetic.main.fragment_chat.*
import org.jivesoftware.smack.ConnectionListener
import org.jivesoftware.smack.XMPPConnection
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Alex.Tran
 */
const val REQUEST_CODE_CHOOSE = 23
const val CAMERA_REQUEST = 1888
const val MY_CAMERA_PERMISSION_CODE = 100
const val GALLERY = 1
const val CAMERA = 2

var isNotDisconnectedRoomChat = false

class ChatFragment : Fragment() {
    lateinit var homeChatViewModel: HomeChatViewModel
    private val messageListener: ChatDialogMessageListener = ChatMessageListener()
    private lateinit var messageSender: ConnectycubeMessageSender
    private val messageStatusListener: MessageStatusListener = ChatMessagesStatusListener()
    private lateinit var permissionsHelper: PermissionsHelper
    private var messAdapter: MessAdapter? = null
    val layoutManager = LinearLayoutManager(context)
    var filePath: File? = null
    var mCameraFileName = ""

    private val onScrollList = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val totalItemCount = layoutManager.itemCount
            val lastVisible = layoutManager.findLastVisibleItemPosition()

            if (totalItemCount - lastVisible == 1) {
                messages_recycleview.removeOnScrollListener(this)
                showProgress()
                homeChatViewModel.loadMore()
            }
        }
    }

    private fun initChatAdapter() {
        val shop = homeChatViewModel.shopInfo
        messAdapter = MessAdapter(
            activity!!,
            shop.avatar,
            shop.fullName,
            homeChatViewModel.chatDialog?.value
        )
        layoutManager.stackFromEnd = false
        layoutManager.reverseLayout = true
        messages_recycleview.layoutManager = layoutManager
        messages_recycleview.adapter = messAdapter
        messages_recycleview.isNestedScrollingEnabled = false
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requestMultiplePermissions()
        showProgress()
        homeChatViewModel = ViewModelProviders.of(activity!!).get(HomeChatViewModel::class.java)
        permissionsHelper = PermissionsHelper(activity!!)

        homeChatViewModel.getDialogChat().observe(this, Observer {
            try {
                it.initForChat(ConnectycubeChatService.getInstance())
                it.addMessageListener(messageListener)
                initChat()
                messageSender = ConnectycubeMessageSender(activity!!, it)
                initManagers()
                initChatAdapter()
            }catch (e: java.lang.Exception){
                startLoginActivity()
            }
        })

        initRoomChat()

        button_chat_send.setOnClickListener {
            onSendChatClick()
        }
        open_gallery.setOnClickListener {
            choosePhotoFromGallery()
        }

        take_photo.setOnClickListener {
            takePhotoFromCamera()
        }

        input_chat_message.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                button_chat_send.setImageResource(
                    if (input_chat_message.text.isNullOrEmpty()) R.drawable.ic_send_gray_24dp else R.drawable.ic_send_black_24dp
                )
            }
        })

        scrim.setOnClickListener {
            hideInputMethod()
        }

        view.viewTreeObserver.addOnGlobalLayoutListener {
            val r =  Rect()
            view.getWindowVisibleDisplayFrame(r)
            val screenHeight = view.rootView.height
            Log.e("ChatFragment", "Keyboard screenHeight----> $screenHeight")
            val heightDiff = screenHeight - (r.bottom - r.top)
            Log.e("ChatFragment", "Keyboard heightDiff----> $heightDiff")
            val visible = heightDiff > screenHeight / 3
            Log.e("ChatFragment", "Keyboard visible----> $visible")
            scrim.visibility = if (visible) View.VISIBLE else View.GONE
        }
    }

    private fun startLoginActivity() {
        activity?.run {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            this.startActivity(intent)
            finish()
        }
    }

    private fun initRoomChat() {
        ConnectycubeChatService.getInstance().addConnectionListener(object : ConnectionListener {

            override fun authenticated(p0: XMPPConnection?, p1: Boolean) {
            }

            override fun connected(connection: XMPPConnection) {
                Log.i("ChatFragment", "room chat is connected!")
                hideProgress()
            }

            override fun connectionClosed() {
                Log.i("ChatFragment", "room chat is closed!")
            }

            override fun connectionClosedOnError(e: Exception) {
                Log.i("ChatFragment", "room chat is error!")
            }

            override fun reconnectingIn(seconds: Int) {
                Log.i("ChatFragment", "reconnecting room chat in $seconds")
                showProgress()
            }

            override fun reconnectionSuccessful() {
                Log.i("ChatFragment", "reconnection room chat is successful")
            }

            override fun reconnectionFailed(e: Exception) {
                Log.i("ChatFragment", "reconnection room chat is failed")
            }
        })

        homeChatViewModel.getChatHistoryStatus().observe(this, Observer {
            hideProgress()
            messAdapter?.messItems = homeChatViewModel.messHistoryList!!
            messAdapter?.notifyDataSetChanged()
            messages_recycleview.addOnScrollListener(onScrollList)
        })

        homeChatViewModel.getChatMessAttachmentStatus().observe(this, Observer {
            if (it) {
                homeChatViewModel.getChatMessAttachmentStatus().value = false
                sendChatMessage(
                    attachment = (homeChatViewModel.getAttachMess())
                )
            }
        })
    }

    private fun hideProgress() {
        activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        loadingMessage.visibility = View.GONE
    }

    fun showProgress() {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        loadingMessage.visibility = View.VISIBLE
    }

    override fun onStart() {
        super.onStart()
        if (isNotDisconnectedRoomChat) {
            isNotDisconnectedRoomChat = false
            return
        }

        ConnectycubeChatService.getInstance().isReconnectionAllowed = true
        if (!ConnectycubeChatService.getInstance().isLoggedIn) {
            showProgress()
            context?.let { context ->
                val user = SharedPreferencesManager.getInstance(context).getCurrentUser()
                ConnectycubeChatService.getInstance().login(user, object : EntityCallback<Void> {
                    override fun onSuccess(void: Void?, bundle: Bundle?) {
                        homeChatViewModel.getDialogChat().observe(this@ChatFragment, Observer {
                            it.initForChat(ConnectycubeChatService.getInstance())
                            it.addMessageListener(messageListener)
                            initChat()
                            messageSender = ConnectycubeMessageSender(activity!!, it)
                            initManagers()
                        })
                        initRoomChat()
                        hideProgress()
                    }

                    override fun onError(ex: ResponseException) {
                        activity?.finish()
                    }
                })
            }
        }
    }

    override fun onStop() {
        super.onStop()
        hideInputMethod()
        if (isNotDisconnectedRoomChat)
            return

        ConnectycubeChatService.getInstance().isReconnectionAllowed = false
        ConnectycubeChatService.getInstance().logout(object : EntityCallback<Void> {
            override fun onSuccess(p0: Void?, p1: Bundle?) {
                ConnectycubeChatService.getInstance().destroy()
            }

            override fun onError(errors: ResponseException) {
            }
        })
    }

    private fun sendFile() {
        isNotDisconnectedRoomChat = false
        var fullName: String
        SharedPreferencesManager.getInstance(context!!).getCurrentUser().apply {
            fullName = this.fullName
        }
        filePath?.let { file ->
            if (file.exists()) {
                val compressedImgFile =
                    Compressor(context).setMaxWidth(1536).setMaxHeight(1152).compressToFile(file)
                UploadAttachment.update(compressedImgFile, homeChatViewModel, fullName)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(activity, "カメラ許可", Toast.LENGTH_LONG).show()
                startActivityForResult(
                    Intent(MediaStore.ACTION_IMAGE_CAPTURE),
                    CAMERA_REQUEST
                )
            } else {
                Toast.makeText(activity, "カメラの許可が拒否されました", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val path = UploadAttachment.getPathFromURI(contentURI, context)
                    if (path != null) {
                        isNotDisconnectedRoomChat = true
                        filePath = File(path)
                        sendFile()
                    }
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }

        } else if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            isNotDisconnectedRoomChat = true
            filePath = File(mCameraFileName)
            sendFile()
        }
    }

    private fun cameraIntent() {
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE

        val newPicFile = "image" + SimpleDateFormat("-mm-ss").format(Date()) + ".jpg"
        val outPath = "/sdcard/$newPicFile"
        val outFile = File(outPath)

        mCameraFileName = outFile.toString()
        val outuri = Uri.fromFile(outFile)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outuri)
        startActivityForResult(intent, CAMERA)
    }

    private fun onSendChatClick() {
        val text = input_chat_message.text.toString().trim()
        if (text.isNotEmpty()) sendChatMessage(text)
    }

    private fun sendChatMessage(text: String = "", attachment: ConnectycubeAttachment? = null) {
        messageSender.sendChatMessage(text, attachment).let {
            if (it.first) {
                if (ConnectycubeDialogType.PRIVATE == homeChatViewModel.chatDialog?.value?.type) {
                    messAdapter?.messItems?.add(0, it.second)
                    messAdapter?.notifyDataSetChanged()
                    messages_recycleview.scrollToPosition(0)
                }
                input_chat_message.setText("")
            } else {
            }
        }

    }

    fun onAttachClick(view: View) {
        if (permissionsHelper.areAllImageGranted()) {
            requestImageDevice()
        } else permissionsHelper.requestImagePermissions()
    }

    private fun requestImageDevice() {
        Matisse.from(activity)
            .choose(MimeType.ofImage(), false)
            .countable(false)
            .capture(true)
            .captureStrategy(
                CaptureStrategy(true, "com.connectycube.messenger.fileprovider")
            )
            .maxSelectable(1)
//                .addFilter(GifSizeFilter(320, 320, 5 * Filter.K * Filter.K))
            .gridExpectedSize(
                resources.getDimensionPixelSize(R.dimen.grid_expected_size)
            )
            .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
            .thumbnailScale(0.85f)
            .imageEngine(Glide4Engine())
            .setOnSelectedListener { uriList, pathList ->
                // DO SOMETHING IMMEDIATELY HERE
            }
            .originalEnable(true)
            .maxOriginalSize(10)
            .setOnCheckedListener {
                // DO SOMETHING IMMEDIATELY HERE
            }
            .forResult(REQUEST_CODE_CHOOSE)
    }

    private fun initChat() {
        when (homeChatViewModel.chatDialog?.value?.type) {
            ConnectycubeDialogType.PRIVATE -> homeChatViewModel.chatDialog?.value?.join(null)
            else -> {
                activity?.finish()
            }
        }
    }

    private fun initManagers() {
        ConnectycubeChatService.getInstance()?.messageStatusesManager?.addMessageStatusListener(
            messageStatusListener
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterChatManagers()
    }

    private fun unregisterChatManagers() {
        ConnectycubeChatService.getInstance()?.messageStatusesManager?.removeMessageStatusListener(
            messageStatusListener
        )
        homeChatViewModel.getDialogChat().value?.removeMessageListrener(messageListener)
//        chatDialog.removeIsTypingListener(messageTypingListener)
    }

    inner class ChatMessageListener : ChatDialogMessageListener {
        override fun processMessage(
            dialogId: String,
            chatMessage: ConnectycubeChatMessage,
            senderId: Int
        ) {
            val isIncoming = senderId != context?.let {
                SharedPreferencesManager.getInstance(it).getCurrentUser().id
            }
            if (isIncoming) {
                messAdapter?.messItems?.add(0, chatMessage)
                messAdapter?.notifyDataSetChanged()
                messages_recycleview.scrollToPosition(0)
            } else {
            }
        }

        override fun processError(
            s: String,
            e: ChatException,
            chatMessage: ConnectycubeChatMessage,
            integer: Int?
        ) {
            println("thanhtd error")
        }
    }


    inner class ChatMessagesStatusListener : MessageStatusListener {
        override fun processMessageRead(messageID: String, dialogId: String, userId: Int) {
            println("thanhtd")
//            modelChatMessageList.updateItemReadStatus(messageID, userId)
        }

        override fun processMessageDelivered(messageID: String, dialogId: String, userId: Int) {
            println("thanhtd da dc gwri thanh cong")
//            modelChatMessageList.updateItemDeliveredStatus(messageID, userId)
        }
    }

    inner class ChatTypingListener : ChatDialogTypingListener {
        override fun processUserIsTyping(dialogId: String?, userId: Int?) {
//            var userStatus = occupants[userId]?.fullName ?: occupants[userId]?.login
//            userStatus?.let {
//                userStatus = getString(R.string.chat_typing, userStatus)
//            }
//            chat_message_members_typing.text = userStatus
//            restartTypingTimer()
        }

        override fun processUserStopTyping(dialogId: String?, userId: Int?) {

        }
    }

    private fun choosePhotoFromGallery() {
        isNotDisconnectedRoomChat = true
        startActivityForResult(
            Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            ), GALLERY
        )
    }

    private fun takePhotoFromCamera() {
        isNotDisconnectedRoomChat = true
        cameraIntent()
    }

    private fun requestMultiplePermissions() {
        Dexter.withActivity(activity)
            .withPermissions(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    // check if all permissions are granted
                    report?.apply {
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            }).withErrorListener {
                Toast.makeText(context, "Some Error! ", Toast.LENGTH_SHORT).show()
            }
            .onSameThread()
            .check()
    }

    fun updateFragmentData() {
        messAdapter?.notifyDataSetChanged()
    }

    fun hideInputMethod() {
        Log.i("ChatFragment", "hideInputMethod")
         Utils.hideInputMethod(input_chat_message)
    }

}