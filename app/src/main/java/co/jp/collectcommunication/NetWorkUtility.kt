package co.jp.collectcommunication

import APPLICATION_ID
import BASE_URL
import android.content.Context
import co.jp.collectcommunication.beans.ApplicationBean
import co.jp.collectcommunication.beans.ChatRoomBean
import co.jp.collectcommunication.beans.CustomerWithoutTokenBean
import co.jp.collectcommunication.pref.ApplicationPref
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface VoidCallBack {
    fun onComplete()
    fun onFailure()
}

interface FcCustomerWithoutTokensBack {
    fun onComplete(bean: CustomerWithoutTokenBean)
    fun onFailure()
}

object NetWorkUtility {

    fun fcMessengerApps(callBack: VoidCallBack, context: Context) {
        val call = retrofit().getApplication(APPLICATION_ID)
        call.enqueue(object : Callback<List<ApplicationBean>> {
            override fun onFailure(call: Call<List<ApplicationBean>>, t: Throwable) {
                t.printStackTrace()
                callBack.onFailure()
            }

            override fun onResponse(
                call: Call<List<ApplicationBean>>,
                response: Response<List<ApplicationBean>>
            ) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        if (it.isNotEmpty()) {
                            val bean = it[0]
                            ApplicationPref.instance.saveApplication(bean, context)
                            callBack.onComplete()
                        } else {
                            callBack.onFailure()
                        }
                    }
                } else {
                    callBack.onFailure()
                }
            }
        })
    }

    fun fcCustomerWithoutTokens(
        accountId: String,
        callBack: FcCustomerWithoutTokensBack,
        context: Context
    ) {
        val call = retrofit().getCustomerWithoutToken(accountId)
        call.enqueue(object : Callback<CustomerWithoutTokenBean> {
            override fun onFailure(call: Call<CustomerWithoutTokenBean>, t: Throwable) {
                t.printStackTrace()
                callBack.onFailure()
            }

            override fun onResponse(
                call: Call<CustomerWithoutTokenBean>,
                response: Response<CustomerWithoutTokenBean>
            ) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        if (it.fc_account_account_id != null) {
                            ApplicationPref.instance.saveCustomerToken(it, context)
                            callBack.onComplete(it)
                        } else {
                            callBack.onFailure()
                        }
                    }
                } else {
                    callBack.onFailure()
                }
            }
        })
    }

    fun fcMessengerAdditionWithoutTokens(
        accountId: String?,
        callBack: VoidCallBack,
        context: Context
    ) {
        val call = retrofit().getChatRoom(accountId)
        call.enqueue(object : Callback<List<ChatRoomBean>> {
            override fun onFailure(call: Call<List<ChatRoomBean>>, t: Throwable) {
                t.printStackTrace()
                callBack.onFailure()
            }

            override fun onResponse(
                call: Call<List<ChatRoomBean>>,
                response: Response<List<ChatRoomBean>>
            ) {
                if (response.isSuccessful) {
                    response.body()?.let {
                        if (it.isNotEmpty()) {
                            val bean = it[0]
                            ApplicationPref.instance.saveChatRoom(bean, context)
                            callBack.onComplete()
                        } else {
                            callBack.onFailure()
                        }
                    }
                } else {
                    callBack.onFailure()
                }
            }
        })
    }

    private fun retrofit(): CollectServer {
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.level = HttpLoggingInterceptor.Level.BODY
        val httpClient = OkHttpClient.Builder()
        // add logging as last interceptor
        httpClient.addInterceptor(logging)  // <-- this is the important line!
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
        return retrofit.create(CollectServer::class.java)
    }
}