package co.jp.collectcommunication

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.budiyev.android.codescanner.*
import com.connectycube.auth.session.ConnectycubeSessionManager
import com.connectycube.core.EntityCallback
import com.connectycube.core.exception.ResponseException
import com.connectycube.users.ConnectycubeUsers
import com.connectycube.users.model.ConnectycubeUser
import co.jp.collectcommunication.pref.SharedPreferencesManager
import com.connectycube.chat.ConnectycubeChatService
import kotlinx.android.synthetic.main.activity_qr_code.loadingProgressBar

const val LOGIN_SUCCESS = 10
const val LOGIN_FAIL = 11

/**
 * Created by Alex.Tran
 */
class QRCodeLoginActivity : AppCompatActivity() {
    private lateinit var codeScanner: CodeScanner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qr_code)
        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)

        codeScanner = CodeScanner(this, scannerView)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                doLogin(it.text)
            }
        }
        codeScanner.errorCallback = ErrorCallback {
            // or ErrorCallback.SUPPRESS
            setResult(LOGIN_FAIL)
            finish()
        }

        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    private fun doLogin(input: String) {
        Log.i("QR_CODE", "raw QR $input")

        val data = input.split("-")
        if (data.size < 3) {
            setResult(LOGIN_FAIL)
            finish()
            return
        }

        val user = ConnectycubeUser()
        user.login = data[1]
        user.password = data[2]
        Log.i("QR_CODE", "login: ${data[1]} | password: ${data[2]} | ShopID: ${data[0]}")

        loadingProgressBar.visibility = View.VISIBLE

        ConnectycubeUsers.signIn(user).performAsync(object : EntityCallback<ConnectycubeUser> {
            override fun onSuccess(user: ConnectycubeUser, args: Bundle) {
                Log.i("QR_CODE", user.toString())

                user.password = ConnectycubeSessionManager.getInstance()
                    .sessionParameters.userPassword

                user.customData = data[0]

                onLogin(user)
            }

            override fun onError(error: ResponseException) {
                loadingProgressBar.visibility = View.GONE
                setResult(LOGIN_FAIL)
                finish()
            }
        })
    }

    private fun onLogin(user: ConnectycubeUser) {
        if (!isLoggedIn) {
            ConnectycubeChatService.getInstance().login(user, object : EntityCallback<Void> {
                override fun onSuccess(void: Void?, bundle: Bundle?) {

                    SharedPreferencesManager.getInstance(applicationContext)
                        .saveCurrentUser(user)

                    loadingProgressBar.visibility = View.GONE
                    setResult(LOGIN_SUCCESS)
                    finish()
                }

                override fun onError(ex: ResponseException) {
                    loadingProgressBar.visibility = View.GONE
                    setResult(LOGIN_FAIL)
                    finish()
                }
            })
        } else {
            loadingProgressBar.visibility = View.GONE
            setResult(LOGIN_SUCCESS)
            finish()
        }
    }

    private val isLoggedIn: Boolean
        get() = ConnectycubeChatService.getInstance().isLoggedIn
}