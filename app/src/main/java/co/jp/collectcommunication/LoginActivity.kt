package co.jp.collectcommunication

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import co.jp.collectcommunication.beans.CustomerWithoutTokenBean
import co.jp.collectcommunication.pref.ApplicationPref
import com.connectycube.chat.ConnectycubeChatService
import com.connectycube.core.EntityCallback
import com.connectycube.core.exception.ResponseException
import com.connectycube.users.ConnectycubeUsers
import com.connectycube.users.model.ConnectycubeUser
import co.jp.collectcommunication.pref.SharedPreferencesManager
import com.bumptech.glide.Glide
import com.connectycube.auth.session.ConnectycubeSessionManager
import com.connectycube.pushnotifications.services.SubscribeService
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.loadingProgressBar

const val QR_CODE_REQUEST_CODE = 1992

class LoginActivity : AppCompatActivity() {

    private val isLoggedIn: Boolean
        get() = ConnectycubeChatService.getInstance().isLoggedIn

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val pInfo = packageManager.getPackageInfo(packageName, 0)
        version.text = "ver ${pInfo.versionName}"
        val bean = ApplicationPref.instance.getApplication(this)

        //Set UI by Server
        Glide.with(this)
            .asDrawable()
            .load(bean.login_background_img)
            .into(background)

        Glide.with(this)
            .asDrawable()
            .load(bean.login_appicon)
            .into(logo_app)

        Glide.with(this)
            .asDrawable()
            .load(bean.login_title)
            .into(login_title)

        SettingsProvider.initConnectycubeCredentials(App.applicationContext(), bean)
        SettingsProvider.initChat()

        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        SubscribeService.subscribeToPushes(applicationContext, true)

        onReLogin()
        btnLogin.setOnClickListener { doLogin() }
        btnLoginQRCode.setOnClickListener {
            //scan QRCode
            requestMultiplePermissions()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == QR_CODE_REQUEST_CODE) {
            if (resultCode == LOGIN_FAIL) {
                displayDialogQRCodeError()
            } else if (resultCode == LOGIN_SUCCESS) {
                loginByQRCode(SharedPreferencesManager.getInstance(this@LoginActivity).getCurrentUser())
            }
        }
    }

    private fun loginByQRCode(user: ConnectycubeUser) {
        directToChatRoom(user)
    }

    private fun displayDialogQRCodeError() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("警告")
        builder.setMessage(R.string.login_by_qr_error)
        builder.setCancelable(false)
        builder.setNegativeButton("はい") { dialog, _ ->
            dialog.dismiss()
        }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    private fun requestMultiplePermissions() {
        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.CAMERA
            )
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    // check if all permissions are granted
                    report?.apply {
                        if (this.areAllPermissionsGranted()) {
                            startActivityForResult(
                                Intent(
                                    this@LoginActivity,
                                    QRCodeLoginActivity::class.java
                                ), QR_CODE_REQUEST_CODE
                            )
                        }
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: MutableList<PermissionRequest>?,
                    token: PermissionToken?
                ) {
                    token?.continuePermissionRequest()
                }
            }).withErrorListener {
                Toast.makeText(
                    this@LoginActivity,
                    "いくつかのエラー！",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .onSameThread()
            .check()
    }

    private fun doLogin() {
        val user = ConnectycubeUser()
        editTextInputLoginId.text.run {
            user.login = this.toString()
        }
        editTextInputPassword.text.run {
            user.password = this.toString()
        }
        if (TextUtils.isEmpty(user.login)
            || TextUtils.isEmpty(user.password)
            || TextUtils.isEmpty(inputShopID.text)
        ) {
            AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("警告")
                .setMessage("すべてのフィールドが空ではありません")
                .setPositiveButton("はい")
                { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }.show()
            return
        } else {
            showProgress()
            signIn(user)
        }
    }

    private fun onReLogin() {
        if (SharedPreferencesManager.getInstance(applicationContext).currentUserExists()) {
            showProgress()
            val user = SharedPreferencesManager.getInstance(applicationContext).getCurrentUser()
            user.customData?.apply {
                inputShopID.setText(this)
            }
            loginClearCache(user)
        } else {
            showSoftKeyboard(inputShopID)
        }
    }

    private fun loginClearCache(user: ConnectycubeUser){
        showProgress()
        ConnectycubeChatService.getInstance().destroy()
        ConnectycubeUsers.signOut().performAsync(object : EntityCallback<Void> {
            override fun onSuccess(p0: Void?, p1: Bundle?) {
                signIn(user)
            }

            override fun onError(p0: ResponseException?) {
                signIn(user)
            }
        })
    }

    private fun signIn(user: ConnectycubeUser){
        ConnectycubeUsers.signIn(user).performAsync(object : EntityCallback<ConnectycubeUser> {
            override fun onSuccess(user: ConnectycubeUser, args: Bundle) {
                Log.i("LoginActivity", user.toString())
                user.password = ConnectycubeSessionManager.getInstance()
                    .sessionParameters.userPassword

                inputShopID.text?.run {
                    user.customData = this.toString()
                }

                onLogin(user)
            }

            override fun onError(error: ResponseException) {
                hideProgress()
                inputShopID.text?.clear()
                Toast.makeText(
                    applicationContext,
                    R.string.login_chat_login_error,
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    fun onLogin(user: ConnectycubeUser) {
        Log.i(
            "LoginActivity",
            "customData: ${user.customData}" +
                    "\n id: ${user.id}" +
                    "\n login: ${user.login}" +
                    "\n password: ${user.password}"
        )
        if (!isLoggedIn) {
            ConnectycubeChatService.getInstance().login(user, object : EntityCallback<Void> {
                override fun onSuccess(void: Void?, bundle: Bundle?) {

                    SharedPreferencesManager.getInstance(applicationContext)
                        .saveCurrentUser(user)
                    directToChatRoom(user)
                }

                override fun onError(ex: ResponseException) {
                    hideProgress()
                    inputShopID.text?.clear()
                    Toast.makeText(
                        applicationContext,
                        R.string.login_chat_login_error,
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        } else {
            directToChatRoom(user)
        }
    }

    fun hideProgress() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        loadingProgressBar.visibility = View.GONE
    }

    private fun showProgress() {
        window.setFlags(
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
        )
        loadingProgressBar.visibility = View.VISIBLE
    }

    private fun directToChatRoom(user: ConnectycubeUser) {
        NetWorkUtility.fcCustomerWithoutTokens(user.login, object : FcCustomerWithoutTokensBack {
            override fun onComplete(bean: CustomerWithoutTokenBean) {
                NetWorkUtility.fcMessengerAdditionWithoutTokens(
                    bean.fc_account_account_id,
                    object : VoidCallBack {
                        override fun onComplete() {
                            hideProgress()
                            SharedPreferencesManager.getInstance(applicationContext)
                                .saveCurrentUser(user)
                            startActivity(Intent(this@LoginActivity, HomeChatActivity::class.java))
                            finish()
                        }

                        override fun onFailure() {
                            onRequestFailure()
                        }

                    },
                    this@LoginActivity
                )
            }

            override fun onFailure() {
                onRequestFailure()
            }

        }, this@LoginActivity)
    }

    fun onRequestFailure() {
        hideProgress()
        Toast.makeText(
            applicationContext,
            "サーバをメンテナンス中ですので、お待ちください。",
            Toast.LENGTH_SHORT
        ).show()
    }


    private fun showSoftKeyboard(view: View) {
        if (view.requestFocus()) {
            val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        }
    }
}


