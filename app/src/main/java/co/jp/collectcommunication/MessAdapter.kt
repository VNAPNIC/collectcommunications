package co.jp.collectcommunication

import android.app.Activity
import android.app.Dialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.jp.collectcommunication.adr.widget.TouchImageView
import co.jp.collectcommunication.pref.SharedPreferencesManager
import co.jp.collectcommunication.util.loadAttachImage
import co.jp.collectcommunication.util.loadChatMessagePhoto
import co.jp.collectcommunication.widget.ClickPattern
import co.jp.collectcommunication.widget.CustomPartialyClickableTextview
import com.amulyakhare.textdrawable.TextDrawable
import com.connectycube.chat.model.ConnectycubeAttachment
import com.connectycube.chat.model.ConnectycubeChatDialog
import com.connectycube.chat.model.ConnectycubeChatMessage
import com.connectycube.chat.model.ConnectycubeDialogType
import java.text.SimpleDateFormat
import java.util.*

private const val IN_PROGRESS = -1
private const val TEXT_OUT_COMING = 1
private const val TEXT_INCOMING = 2
private const val ATTACH_IMAGE_OUT_COMING = 3
private const val ATTACH_IMAGE_INCOMING = 4

/**
 * Created by Alex.Tran
 */
class MessAdapter(
    val context: Activity,
    private val avatarShop: String?,
    val shopName: String?,
    val chatDialog: ConnectycubeChatDialog?
) :
    PagedListAdapter<ConnectycubeChatMessage, RecyclerView.ViewHolder>(diffCallback) {

    //RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    fun formatTime(seconds: Long): String {
        val date = Date(seconds * 1000)
        val df2 = SimpleDateFormat("hh:mm a")
        return df2.format(date)
    }

    fun formatDate(seconds: Long): String {
        val date = Date(seconds * 1000)
        val df2 = SimpleDateFormat("MM/dd")
        return df2.format(date)
    }

    companion object {
        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         */
        private val PAYLOAD_STATUS = Any()
        private val diffCallback = object : DiffUtil.ItemCallback<ConnectycubeChatMessage>() {
            override fun areItemsTheSame(
                oldItem: ConnectycubeChatMessage,
                newItem: ConnectycubeChatMessage
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ConnectycubeChatMessage,
                newItem: ConnectycubeChatMessage
            ): Boolean =
                oldItem.id == newItem.id && oldItem.readIds == newItem.readIds && oldItem.deliveredIds == newItem.deliveredIds

            override fun getChangePayload(
                oldItem: ConnectycubeChatMessage,
                newItem: ConnectycubeChatMessage
            ): Any? {
                return if (sameExceptStatus(oldItem, newItem)) {
                    PAYLOAD_STATUS
                } else null
            }

            fun sameExceptStatus(
                oldItem: ConnectycubeChatMessage,
                newItem: ConnectycubeChatMessage
            ): Boolean {
                return newItem.readIds != oldItem.readIds || newItem.deliveredIds != oldItem.deliveredIds
            }
        }
    }

    var messItems: ArrayList<ConnectycubeChatMessage> = arrayListOf()


    private val localUserId = SharedPreferencesManager.getInstance(context).getCurrentUser().id
    val occupantIds = chatDialog?.occupants?.apply { remove(localUserId) }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TEXT_OUT_COMING -> ChatMessageOutComingViewHolder(parent, R.layout.chat_outcoming_item)
            TEXT_INCOMING -> ChatMessageIncomingViewHolder(parent, R.layout.chat_incoming_item)
            ATTACH_IMAGE_OUT_COMING -> ChatImageAttachOutComingViewHolder(
                parent,
                R.layout.chat_outcoming_attachimage_item
            )
            ATTACH_IMAGE_INCOMING -> ChatImageAttachIncomingViewHolder(
                parent,
                R.layout.chat_incoming_attachimage_item
            )
            IN_PROGRESS -> NetworkStateItemViewHolder.create(parent)
            else -> throw IllegalArgumentException("Wrong type of viewType= $viewType")
        }
    }

    override fun getItemCount(): Int {
        return messItems.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ChatMessageOutComingViewHolder -> holder.bindTo(messItems[position])
            is ChatMessageIncomingViewHolder -> holder.bindTo(messItems[position])
            is ChatImageAttachIncomingViewHolder -> holder.bindTo(messItems[position])
            is ChatImageAttachOutComingViewHolder -> holder.bindTo(messItems[position])

        }
    }

    //========================= OutComing =========================================//

    inner class ChatImageAttachOutComingViewHolder(
        val parent: ViewGroup,
        @LayoutRes chatItem: Int
    ) :
        BaseChatImageAttachViewHolder(parent, chatItem) {
        private val imgAvatar: ImageView = itemView.findViewById(R.id.avatar_image_view)
        private val outcome: TextView = itemView.findViewById(R.id.timeSentOutcomeAttach)
        private val dateOutCome: TextView = itemView.findViewById(R.id.dateSentOutcomeAttach)
        override fun bindTo(message: ConnectycubeChatMessage) {
            super.bindTo(message)
            val user = SharedPreferencesManager.getInstance(parent.context).getCurrentUser()
            if (user.avatar.isNullOrEmpty() && user.fullName != null && user.fullName.isNotEmpty()) {
                imgAvatar.setImageDrawable(getAvatarByName(user.fullName))
            } else {
                loadChatMessagePhoto(
                    chatDialog?.type == ConnectycubeDialogType.PRIVATE,
                    user.avatar,
                    imgAvatar,
                    context
                )
            }

            message.dateSent.apply {
                outcome.text = formatTime(this)
                dateOutCome.text = formatDate(this)
            }
        }
    }

    inner class ChatMessageOutComingViewHolder(val parent: ViewGroup, @LayoutRes chatItem: Int) :
        BaseChatMessageTextViewHolder(parent, chatItem) {

        private val imgAvatar: ImageView = itemView.findViewById(R.id.avatar_image_view)
        private val textMess: CustomPartialyClickableTextview =
            itemView.findViewById(R.id.text_message_body)
        private val timeOutCome: TextView = itemView.findViewById(R.id.timeSentOutcome)
        private val dateOutCome: TextView = itemView.findViewById(R.id.dateSentOutcome)
        override fun bindTo(message: ConnectycubeChatMessage) {
            super.bindTo(message)
            val user = SharedPreferencesManager.getInstance(parent.context).getCurrentUser()
            if (user.avatar.isNullOrEmpty() && user.fullName != null && user.fullName.isNotEmpty()) {
                imgAvatar.setImageDrawable(getAvatarByName(user.fullName))
            } else {
                loadChatMessagePhoto(
                    chatDialog?.type == ConnectycubeDialogType.PRIVATE,
                    user.avatar,
                    imgAvatar,
                    context
                )
            }

            textMess.text = message.body
            textMess.setOnLongClickListener {
                displayPopupWindow(it as CustomPartialyClickableTextview)
                true
            }
            /**
             * Create Objects For Click Patterns
             */
            val phone = ClickPattern()
            val weblink = ClickPattern()

            /**
             * set Functionality for what will happen on click of that pattern
             * In this example pattern is phone
             */
            phone.onClickListener = object : ClickPattern.OnClickListener {
                override fun onClick(str: String) {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:$str")
                    itemView.context.startActivity(intent)
                }

            }

            /**
             * set Functionality for what will happen on click of that pattern
             * In this example pattern is weblink
             */
            weblink.onClickListener = object : ClickPattern.OnClickListener {
                override fun onClick(str: String) {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(str)
                    itemView.context.startActivity(intent)
                }
            }

            /**
             * set respective regex string to be used to identify patter
             */
            phone.regex =
                "\\+?\\(?([0-9]{3})\\)?[-.]?\\(?([0-9]{3})\\)?[-.]?\\(?([0-9]{4})\\)?" // regex for phone number
            weblink.regex =
                "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]" // regex for weblink

            /**
             * add click pattern to the custom textview - first parameter is tag for reference second parameter is ClickPattern object
             */
            textMess.addClickPattern("phone", phone)
            textMess.addClickPattern("weblink", weblink)

            message.dateSent.apply {
                timeOutCome.text = formatTime(this)
                dateOutCome.text = formatDate(this)
            }
        }
    }

    //========================= Incoming =========================================//

    inner class ChatImageAttachIncomingViewHolder(parent: ViewGroup, @LayoutRes chatItem: Int) :
        BaseChatImageAttachViewHolder(parent, chatItem) {
        private val imgAvatar: ImageView = itemView.findViewById(R.id.avatar_image_view)
        private val inCome: TextView = itemView.findViewById(R.id.timeSentIncomeAttach)
        private val dateInCome: TextView = itemView.findViewById(R.id.dateSentIncomeAttach)
        override fun bindTo(message: ConnectycubeChatMessage) {
            super.bindTo(message)

            Log.i("Incoming", "-------> avatarShop: $avatarShop - shopName: $shopName")

            if (avatarShop.isNullOrEmpty() && shopName != null && shopName.isNotEmpty()) {
                imgAvatar.setImageDrawable(getAvatarByName(shopName))
            } else {
                loadChatMessagePhoto(
                    chatDialog?.type == ConnectycubeDialogType.PRIVATE,
                    avatarShop,
                    imgAvatar,
                    context
                )
            }

            message.dateSent.apply {
                inCome.text = formatTime(this)
                dateInCome.text = formatDate(this)
            }
        }
    }

    inner class ChatMessageIncomingViewHolder(parent: ViewGroup, @LayoutRes chatItem: Int) :
        BaseChatMessageTextViewHolder(parent, chatItem) {
        private val imgAvatar: ImageView = itemView.findViewById(R.id.avatar_image_view)
        private val textMess: CustomPartialyClickableTextview =
            itemView.findViewById(R.id.text_message_body)
        private val timeIncome: TextView = itemView.findViewById(R.id.timeSentIncome)
        private val dateIncome: TextView = itemView.findViewById(R.id.dateSentIncome)

        override fun bindTo(message: ConnectycubeChatMessage) {
            super.bindTo(message)

            Log.i("Incoming", "-------> avatarShop: $avatarShop - shopName: $shopName")

            if (avatarShop.isNullOrEmpty() && shopName != null && shopName.isNotEmpty()) {
                imgAvatar.setImageDrawable(getAvatarByName(shopName))
            } else {
                loadChatMessagePhoto(
                    chatDialog?.type == ConnectycubeDialogType.PRIVATE,
                    avatarShop,
                    imgAvatar,
                    context
                )
            }

            textMess.text = message.body

            textMess.setOnLongClickListener {
                displayPopupWindow(it as CustomPartialyClickableTextview)
                true
            }
            /**
             * Create Objects For Click Patterns
             */
            val phone = ClickPattern()
            val weblink = ClickPattern()

            /**
             * set Functionality for what will happen on click of that pattern
             * In this example pattern is phone
             */
            phone.onClickListener = object : ClickPattern.OnClickListener {
                override fun onClick(str: String) {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:$str")
                    itemView.context.startActivity(intent)
                }

            }

            /**
             * set Functionality for what will happen on click of that pattern
             * In this example pattern is weblink
             */
            weblink.onClickListener = object : ClickPattern.OnClickListener {
                override fun onClick(str: String) {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(str)
                    itemView.context.startActivity(intent)
                }
            }

            /**
             * set respective regex string to be used to identify patter
             */
            phone.regex =
                "\\+?\\(?([0-9]{3})\\)?[-.]?\\(?([0-9]{3})\\)?[-.]?\\(?([0-9]{4})\\)?" // regex for phone number
            weblink.regex =
                "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]" // regex for weblink

            /**
             * add click pattern to the custom textview - first parameter is tag for reference second parameter is ClickPattern object
             */
            textMess.addClickPattern("phone", phone)
            textMess.addClickPattern("weblink", weblink)

            message.dateSent.apply {
                timeIncome.text = formatTime(this)
                dateIncome.text = formatDate(this)
            }
        }
    }

    fun getAvatarByName(name: String): TextDrawable {
        val subName = if (name.length > 1) name.substring(0, 2) else name
        return TextDrawable.builder()
            .buildRound(subName, Color.parseColor("#65a7c1"))
    }

    //========================= Base =========================================//

    open inner class BaseChatImageAttachViewHolder(parent: ViewGroup, @LayoutRes chatItem: Int) :
        BaseChatMessageViewHolder(
            LayoutInflater.from(parent.context).inflate(chatItem, parent, false)
        ) {
        private val attachmentView: TouchImageView =
            itemView.findViewById(R.id.attachment_image_view)

        override fun bindTo(message: ConnectycubeChatMessage) {
            super.bindTo(message)
            showImageAttachment(message)
        }

        private fun showImageAttachment(message: ConnectycubeChatMessage?) {
            if (message?.attachments != null) {
                val validUrl = getAttachImageUrl(message.attachments.iterator().next())
                loadAttachImage(validUrl, attachmentView, context)
                attachmentView.setOnClickListener {

                    val manager = (context as AppCompatActivity).supportFragmentManager
                    val transaction = manager.beginTransaction()
                    transaction.setCustomAnimations(
                        android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right
                    )
                    transaction.add(
                        R.id.framelayout,
                        DisplayImageFragment(validUrl),
                        DisplayImageFragment::class.java.canonicalName
                    )
                    transaction.addToBackStack(DisplayImageFragment::class.java.canonicalName)
                    transaction.commit()
                }
            }
        }
    }

    open inner class BaseChatMessageViewHolder(@NonNull itemView: View) : RecyclerView.ViewHolder(
        itemView
    ) {
//        val dateView = itemView.findViewById<TextView>(R.id.text_message_date)
        /**
         * Items might be null if they are not paged in yet. PagedListAdapter will re-bind the
         * ViewHolder when Item is loaded.
         */
        open fun bindTo(message: ConnectycubeChatMessage) {
//            dateView.text = formatDate(message.dateSent)
        }
    }

    open inner class BaseChatMessageTextViewHolder(parent: ViewGroup, @LayoutRes chatItem: Int) :
        BaseChatMessageViewHolder(
            LayoutInflater.from(parent.context).inflate(chatItem, parent, false)
        ) {
        private val bodyView = itemView.findViewById<TextView>(R.id.text_message_body)
        private var message: ConnectycubeChatMessage? = null

    }

    override fun getItemViewType(position: Int): Int {
        val chatMessage = messItems[position]
        chatMessage.let {
            val isReceived = isIncoming(chatMessage)
            return if (withAttachment(chatMessage)) {
                if (isReceived) {
                    ATTACH_IMAGE_INCOMING
                } else ATTACH_IMAGE_OUT_COMING
            } else if (isReceived) {
                TEXT_INCOMING
            } else TEXT_OUT_COMING
        }
    }

    private fun isIncoming(chatMessage: ConnectycubeChatMessage): Boolean {
        val localUser = SharedPreferencesManager.getInstance(context).getCurrentUser()
        return chatMessage.senderId != null && chatMessage.senderId != localUser.id
    }

    private fun withAttachment(chatMessage: ConnectycubeChatMessage): Boolean {
        val attachments = chatMessage.attachments
        return attachments != null && !attachments.isEmpty()
    }

    fun getAttachImageUrl(attachment: ConnectycubeAttachment): String? {
        return attachment.url
    }


    private fun displayPopupWindow(anchorView: CustomPartialyClickableTextview) {
        val dialog = Dialog(anchorView.context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        val inflater =
            anchorView.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layout: View = inflater.inflate(R.layout.popup_content, null)
        layout.findViewById<TextView>(R.id.copy).setOnClickListener {
            val cManager =
                anchorView.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val cData = ClipData.newPlainText("text", anchorView.text)
            cManager.primaryClip = cData
            Toast.makeText(
                anchorView.context,
                "Copied to clipboard : ${cManager.primaryClip?.getItemAt(0)?.text}",
                Toast.LENGTH_SHORT
            ).show()
            dialog.dismiss()
        }
        layout.findViewById<TextView>(R.id.cancel).setOnClickListener {
            dialog.dismiss()
        }

        dialog.setContentView(layout)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()
    }
}