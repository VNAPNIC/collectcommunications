package co.jp.collectcommunication;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;

import co.jp.collectcommunication.adr.widget.TouchImageView;

public class DisplayImageFragment extends Fragment {

    private String url;

    public DisplayImageFragment() {
    }

    public DisplayImageFragment(String url) {
        this.url = url;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_image, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().findViewById(R.id.headerBar).setVisibility(View.GONE);
        if (!TextUtils.isEmpty(url)) {
            TouchImageView imageViewZoom = view.findViewById(R.id.imageview_item);
            Glide.with(this).load(url)
                    .fitCenter()
                    .into(imageViewZoom);
        }
        view.findViewById(R.id.close_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().findViewById(R.id.headerBar).setVisibility(View.VISIBLE);
                getActivity().onBackPressed();
            }
        });
    }

    @Override
    public void onDestroy() {
        getActivity().findViewById(R.id.headerBar).setVisibility(View.VISIBLE);
        super.onDestroy();

    }
}