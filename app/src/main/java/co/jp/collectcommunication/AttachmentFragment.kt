package co.jp.collectcommunication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import co.jp.collectcommunication.adapter.AttachmentAdapter
import com.connectycube.chat.model.ConnectycubeChatMessage
import com.connectycube.core.EntityCallback
import com.connectycube.core.exception.ResponseException
import kotlinx.android.synthetic.main.fragmnent_attachment_history.*

/**
 * Created by Alex.Tran
 */
class AttachmentFragment : Fragment() {
    lateinit var homeChatViewModel: HomeChatViewModel
    lateinit var attachAdapter: AttachmentAdapter
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragmnent_attachment_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.findViewById<ImageButton>(R.id.imageMenu)?.visibility = View.INVISIBLE
        homeChatViewModel = ViewModelProviders.of(activity!!).get(HomeChatViewModel::class.java)
        initChatAdapter()
        llBottom.setOnClickListener {
            activity?.findViewById<ImageButton>(R.id.imageMenu)?.visibility = View.VISIBLE
            activity?.onBackPressed()
        }
    }

    private fun initChatAdapter() {
        attachAdapter = AttachmentAdapter(activity!!,homeChatViewModel.items)

        val mLayoutManager = GridLayoutManager(activity, 3)
        mLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (attachAdapter.getItemViewType(position) == 1) {
                    print("getSpanSize ---------> ${attachAdapter.getItemViewType(position)}")
                    3
                } else {
                    1
                }
            }
        }

        attachment_messages_recycleview.layoutManager = mLayoutManager
        val spacingInPixels = resources.getDimensionPixelSize(R.dimen.spacing)
        attachment_messages_recycleview.addItemDecoration(SpacesItemDecoration(spacingInPixels))
        attachment_messages_recycleview.adapter = attachAdapter

        //add old data
        attachAdapter.items.clear()
        attachAdapter.items.addAll(homeChatViewModel.items)

        //get new data
        refreshData.isRefreshing = true
        getChatHistory()
        refreshData.setOnRefreshListener {
            getChatHistory()
        }

    }

    private fun getChatHistory(){
        homeChatViewModel.getChatHistory(object :
            EntityCallback<ArrayList<ConnectycubeChatMessage>> {
            override fun onSuccess(
                messages: ArrayList<ConnectycubeChatMessage>,
                bundle: Bundle
            ) {
                homeChatViewModel.items.clear()
                homeChatViewModel.getAttachment(messages)

                attachAdapter.notifyDataSetChanged()

                refreshData?.isRefreshing = false
            }

            override fun onError(error: ResponseException) {
                refreshData?.isRefreshing = false
                error.printStackTrace()
            }
        })
    }
}