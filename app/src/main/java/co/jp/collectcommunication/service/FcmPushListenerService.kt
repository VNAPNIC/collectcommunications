package co.jp.collectcommunication.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.util.Log
import co.jp.collectcommunication.helpers.AppNotificationManager
import com.connectycube.pushnotifications.services.fcm.FcmPushListenerService
import com.google.firebase.messaging.RemoteMessage

class FcmPushListenerService : FcmPushListenerService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.i("FcmPushListenerService","-----------------------------------------------------------> FcmPushListenerService")
        AppNotificationManager.getInstance().processPushNotification(this, remoteMessage.data)
    }

    override fun onTaskRemoved(rootIntent: Intent) {
        super.onTaskRemoved(rootIntent)
        val restartService = Intent(
            applicationContext,
            this.javaClass
        )
        restartService.setPackage(packageName)
        val restartServicePI = PendingIntent.getService(
            applicationContext, 1, restartService,
            PendingIntent.FLAG_ONE_SHOT
        )
        //Restart the service once it has been killed android
        val alarmService =
            applicationContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmService?.set(
            AlarmManager.ELAPSED_REALTIME,
            SystemClock.elapsedRealtime() + 100,
            restartServicePI
        )
    }
}