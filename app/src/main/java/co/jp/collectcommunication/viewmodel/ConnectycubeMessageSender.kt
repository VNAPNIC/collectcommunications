package co.jp.collectcommunication.viewmodel

import android.content.Context
import android.util.Log
import android.widget.Toast
import co.jp.collectcommunication.R
import com.connectycube.chat.model.ConnectycubeAttachment
import com.connectycube.chat.model.ConnectycubeChatDialog
import com.connectycube.chat.model.ConnectycubeChatMessage
import com.connectycube.chat.model.ConnectycubeDialogType
import org.jivesoftware.smack.SmackException

class ConnectycubeMessageSender(private val context: Context, private val dialog: ConnectycubeChatDialog) {


    fun sendChatMessage(
        text: String? = "",
        attachment: ConnectycubeAttachment? = null
    ): Pair<Boolean, ConnectycubeChatMessage> {
        val chatMessage = ConnectycubeChatMessage()
        var result = false
        if (attachment != null) {
            chatMessage.addAttachment(attachment)
            chatMessage.body = context.getString(R.string.message_attachment)
        } else {
            chatMessage.body = text
        }
        chatMessage.setSaveToHistory(true)
        chatMessage.dateSent = System.currentTimeMillis() / 1000
        chatMessage.isMarkable = true

        if (ConnectycubeDialogType.PRIVATE != dialog.type && !dialog.isJoined) {
            Log.i("MessageSender", "You're not joined a group chat yet, try again later")
            return Pair(result, chatMessage)
        }

        try {
            dialog.sendMessage(chatMessage)
            result = true
        } catch (e: SmackException.NotConnectedException) {
            Log.i("MessageSender", "Can't send a message, You are not connected to chat")
        } catch (e: InterruptedException) {
            Log.i("MessageSender",  "Can't send a message, You are not connected to chat")
        } finally {
            return Pair(result, chatMessage)
        }
    }

}