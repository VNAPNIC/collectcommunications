package co.jp.collectcommunication.pref

import android.content.Context
import android.content.Context.MODE_PRIVATE
import co.jp.collectcommunication.beans.ApplicationBean
import co.jp.collectcommunication.beans.ChatRoomBean
import co.jp.collectcommunication.beans.CustomerWithoutTokenBean

//fc-messenger-apps
const val KEY_LOGIN_APP_ICON = "key.login.app.icon"
const val KEY_LOGIN_BACKGROUND_IMG = "key.login.background.img"
const val KEY_LOGIN_TITLE = "key.login.title"
const val KEY_CHAT_ID = "key.chat.id"
const val KEY_AUTHKEY = "key.authkey"
const val KEY_AUTHSECRET = "key.authsecret"
const val KEY_ACCOUNTKEY = "key.accountkey"

//fc-messenger-addition-without-tokens
const val KEY_ACCOUNT_ID = "key.account.id"
const val KEY_MAIN_TOP_IMG = "key.main.top.img"
const val KEY_MAIN_TOP_TITLE = "key.main.top.title"
const val KEY_MAIN_TOP_ORDER = "key.main.top.order"
const val KEY_MAIN_TOP_SHOPPING = "key.main.top.shopping"
const val KEY_MAIN_TOP_SHOPPING_INFO = "key.main.top.shopping.info"

const val FC_ACCOUNT_ACCOUNT_ID = "fc.account.account.id"

class ApplicationPref private constructor() {
    companion object {
        val instance = ApplicationPref()
    }

    fun saveApplication(application: ApplicationBean, context: Context) {
        val pref = context.getSharedPreferences("PREF_APPLICATION", MODE_PRIVATE)
        val editor = pref.edit()

        editor.putString(KEY_LOGIN_APP_ICON, application.login_appicon)
        editor.putString(KEY_LOGIN_BACKGROUND_IMG, application.login_background_img)
        editor.putString(KEY_LOGIN_TITLE, application.login_title)
        editor.putInt(KEY_CHAT_ID, application.chat_id)
        editor.putString(KEY_AUTHKEY, application.chat_authkey)
        editor.putString(KEY_AUTHSECRET, application.chat_authsecret)
        editor.putString(KEY_ACCOUNTKEY, application.chat_accountkey)

        editor.apply()
    }

    fun getApplication(context: Context): ApplicationBean {
        val pref = context.getSharedPreferences("PREF_APPLICATION", MODE_PRIVATE)
        return ApplicationBean(
            pref.getString(KEY_LOGIN_APP_ICON, ""),
            pref.getString(KEY_LOGIN_BACKGROUND_IMG, ""),
            pref.getString(KEY_LOGIN_TITLE, ""),
            pref.getInt(KEY_CHAT_ID, -1),
            pref.getString(KEY_AUTHKEY, ""),
            pref.getString(KEY_AUTHSECRET, ""),
            pref.getString(KEY_ACCOUNTKEY, "")
        )
    }


    fun saveChatRoom(bean: ChatRoomBean, context: Context) {
        val pref = context.getSharedPreferences("PREF_CHAT_ROOM", MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(KEY_ACCOUNT_ID, bean.account_id)
        editor.putString(KEY_MAIN_TOP_IMG, bean.main_top_img)
        editor.putString(KEY_MAIN_TOP_TITLE, bean.main_top_title)
        editor.putString(KEY_MAIN_TOP_ORDER, bean.main_menu_order)
        editor.putString(KEY_MAIN_TOP_SHOPPING, bean.main_menu_shopping)
        editor.putString(KEY_MAIN_TOP_SHOPPING_INFO, bean.main_menu_shopinfo)
        editor.apply()
    }

    fun getChatRoom(context: Context): ChatRoomBean {
        val pref = context.getSharedPreferences("PREF_CHAT_ROOM", MODE_PRIVATE)
        return ChatRoomBean(
            pref.getString(KEY_ACCOUNT_ID, ""),
            pref.getString(KEY_MAIN_TOP_IMG, ""),
            pref.getString(KEY_MAIN_TOP_TITLE, ""),
            pref.getString(KEY_MAIN_TOP_ORDER, ""),
            pref.getString(KEY_MAIN_TOP_SHOPPING, ""),
            pref.getString(KEY_MAIN_TOP_SHOPPING_INFO, "")
        )
    }

    fun saveCustomerToken(bean: CustomerWithoutTokenBean,context: Context){
        val pref = context.getSharedPreferences("PREF_CUSTOMER_TOKEN", MODE_PRIVATE)
        val editor = pref.edit()
        editor.putString(FC_ACCOUNT_ACCOUNT_ID, bean.fc_account_account_id)
        editor.apply()
    }

    fun  getCustomerToken(context: Context): CustomerWithoutTokenBean{
        val pref = context.getSharedPreferences("PREF_CUSTOMER_TOKEN", MODE_PRIVATE)
        return CustomerWithoutTokenBean(
            pref.getString(FC_ACCOUNT_ACCOUNT_ID, "")
        )
    }

    fun clear(context:Context) {
        val applicationPref = context.getSharedPreferences("PREF_APPLICATION", MODE_PRIVATE)
        val chatRoomPref = context.getSharedPreferences("PREF_CHAT_ROOM", MODE_PRIVATE)
        val customerPref = context.getSharedPreferences("PREF_CUSTOMER_TOKEN", MODE_PRIVATE)

        applicationPref.edit().clear().apply()
        chatRoomPref.edit().clear().apply()
        customerPref.edit().clear().apply()
    }
}