package co.jp.collectcommunication

import co.jp.collectcommunication.beans.ApplicationBean
import co.jp.collectcommunication.beans.ChatRoomBean
import co.jp.collectcommunication.beans.CustomerWithoutTokenBean
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface CollectServer {
    @GET("fc-messenger-apps?")
    fun getApplication(@Query("application_id")applicationId:Int): Call<List<ApplicationBean>>

    @GET("fc-messenger-addition-without-tokens?")
    fun getChatRoom(@Query("account_id")fcAccountId:String?): Call<List<ChatRoomBean>>

    @GET("fc-customer-without-tokens/{account_id}")
    fun getCustomerWithoutToken(@Path("account_id")accountId:String?): Call<CustomerWithoutTokenBean>
}