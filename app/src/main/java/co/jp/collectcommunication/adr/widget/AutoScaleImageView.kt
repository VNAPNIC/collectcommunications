package com.vincommerce.adayroi.base.widget

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import co.jp.collectcommunication.R

/**
 * Created by Le Ngoc on 8/5/2019
 * Copyright © 2019 ADAYROI
 **/
class AutoScaleImageView : AppCompatImageView {

    var ratio: Float = 0f // = width/height
        set(value) {
            field = if(value > 0) value else 0f
            requestLayout()
            invalidate()
        }

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        init(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyle: Int) : super(context, attributeSet, defStyle) {
        init(context, attributeSet)
    }

    private fun init(ctx: Context, attrs: AttributeSet?) {
        attrs?.apply {
            val a = ctx.obtainStyledAttributes(attrs, R.styleable.AutoScaleImageView)
            ratio = a.getFloat(R.styleable.AutoScaleImageView_ratio, 0f)
            if (ratio < 0) {
                ratio = 0f
            }
            a.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (ratio > 0) {
            scaleType = ScaleType.CENTER_CROP
            var widthSize = MeasureSpec.getSize(widthMeasureSpec)
            val widthMode = MeasureSpec.getMode(widthMeasureSpec)
            val heightMode = MeasureSpec.getMode(heightMeasureSpec)
            var heightSize = MeasureSpec.getSize(heightMeasureSpec)
            if(widthMode == MeasureSpec.EXACTLY || heightMode == MeasureSpec.EXACTLY) {
                if(widthMode == MeasureSpec.EXACTLY) {
                   heightSize = (widthSize / ratio).toInt()
                } else {
                    widthSize = (heightSize * ratio).toInt()
                }
            } else {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec)
                heightSize = (measuredWidth / ratio).toInt()
            }
            setMeasuredDimension(widthSize, heightSize)
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }
}