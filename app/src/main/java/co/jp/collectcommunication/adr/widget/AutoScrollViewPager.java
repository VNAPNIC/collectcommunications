package co.jp.collectcommunication.adr.widget;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import java.util.Timer;
import java.util.TimerTask;


public class AutoScrollViewPager extends ViewPager {

    Timer mTimer;
    private boolean isTouchDown = false;

    public AutoScrollViewPager(Context context) {
        super(context);
        init();

    }

    public AutoScrollViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    private void init() {
        addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (state == SCROLL_STATE_IDLE) {
                    isTouchDown = false;
                } else {
                    isTouchDown = true;
                }

            }
        });
    }

    public void setAutoScrollEnabled(int duration) {
        if (mTimer != null) {
            mTimer.cancel();
        }
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!isTouchDown) {
                    post(new Runnable() {
                        @Override
                        public void run() {
                            if (getCurrentItem() == (getAdapter().getCount() - 1)) {
                                setCurrentItem(0, false);
                            } else {
                                setCurrentItem(getCurrentItem() + 1, true);
                            }
                        }
                    });

                }

            }
        }, duration, duration);
    }

    public void setDisableAutoScrollEnabled() {
        if (mTimer != null) {
            mTimer.cancel();
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        setDisableAutoScrollEnabled();
    }

    @Override
    public void addOnPageChangeListener(@NonNull OnPageChangeListener listener) {
        super.addOnPageChangeListener(listener);
    }

}
