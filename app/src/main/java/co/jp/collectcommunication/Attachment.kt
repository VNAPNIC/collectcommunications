package co.jp.collectcommunication

import com.connectycube.chat.model.ConnectycubeAttachment

enum class AttachmentType {
    TITLE,
    ITEM
}

data class Attachment(
    val type: AttachmentType,
    val title: String,
    val items: ConnectycubeAttachment?
)