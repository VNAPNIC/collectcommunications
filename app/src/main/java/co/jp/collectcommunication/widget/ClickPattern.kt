package co.jp.collectcommunication.widget

class ClickPattern {

    /**
     * Regex String to find pattern
     */
    var regex: String? = null

    /**
     * Click Listener to give implementation of onClick
     * on the pattern. For ex: to print toast of "Email Clicked "
     * on click of emails you have to give an implementation for this
     */
    var onClickListener: OnClickListener? = null


    /**
     * Interface for click
     */
    interface OnClickListener {

        fun onClick(str:String)
    }
}