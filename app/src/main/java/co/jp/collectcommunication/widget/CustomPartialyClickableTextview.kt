package co.jp.collectcommunication.widget

import android.content.Context
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.AttributeSet
import android.view.View
import android.widget.TextView

import java.util.ArrayList
import java.util.HashMap
import java.util.Map
import java.util.regex.Pattern

class CustomPartialyClickableTextview : TextView {

    /**
     * hashmap to hold clickPatterns objects
     */
    var clickPatterns: HashMap<String, ClickPattern>? = HashMap()


    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context, attrs)

    }

    /**
     * Initialize fields.
     *
     * @param context
     * @param attrs
     */
    private fun init(context: Context, attrs: AttributeSet) {
        makePatternsClickable(this)
    }

    /**
     * Initialize fields.
     *
     * @param context
     */
    private fun init(context: Context) {
        makePatternsClickable(this)
    }

    /**
     * Make Spannables for respective Strings and add onClick methods respectively
     * @param textView
     */
    private fun makePatternsClickable(textView: TextView) {
        val it = clickPatterns!!.entries.iterator() // take out iterator of clickPatterns
        while (it.hasNext()) {  // Iterate through all the added patterns one by one
            val pair = it.next() as Map.Entry<*, *>
            val clickPattern = pair.value as ClickPattern
            /**
             * patternsFromStrung will contain all the found patterns
             */
            val patternsFromString =
                getPatternFromString(textView.text.toString(), clickPattern.regex)
            for ((i, s) in patternsFromString.withIndex()) {
                MakeSpannableStrings(textView, clickPattern, patternsFromString, i, s)
            }
        }
    }

    /**
     * code to make pattern clickable and add respective onClick implementation
     * @param textView
     * @param clickPattern
     * @param emailsFromString
     * @param i
     */
    private fun MakeSpannableStrings(
        textView: TextView,
        clickPattern: ClickPattern,
        emailsFromString: ArrayList<String>,
        i: Int,
        s: String
    ) {
        val ss = SpannableString(textView.text)
        val span1 = object : ClickableSpan() {
            override fun onClick(textView: View) {
                clickPattern.onClickListener?.onClick(s)
            }
        }
        ss.setSpan(
            span1,
            textView.text.toString().indexOf(emailsFromString[i]),
            textView.text.toString().indexOf(
                emailsFromString[i]
            ) + emailsFromString[i].length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        textView.text = ss
        textView.movementMethod = LinkMovementMethod.getInstance()
    }


    /**
     * parsing pattern
     *
     * @param text
     * @return arraylist of emails
     */
    private fun getPatternFromString(text: String, regex: String?): ArrayList<String> {
        val p = Pattern.compile(
            regex!!,
            Pattern.CASE_INSENSITIVE
        )
        val matcher = p.matcher(text)
        val patterns = ArrayList<String>()

        while (matcher.find()) {
            patterns.add(matcher.group())
        }

        println(patterns)

        return patterns

    }


    fun addClickPattern(tag: String, clickPattern: ClickPattern) {
        if (clickPatterns != null) {
            clickPatterns!![tag] = clickPattern
            makePatternsClickable(this)
        }
    }
}