package co.jp.collectcommunication.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.connectycube.chat.model.ConnectycubeAttachment;
import com.connectycube.core.EntityCallback;
import com.connectycube.core.exception.ResponseException;
import com.connectycube.storage.ConnectycubeStorage;
import com.connectycube.storage.model.ConnectycubeFile;
import co.jp.collectcommunication.HomeChatViewModel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Alex.Tran
 */
public class UploadAttachment {
    public static void update(File file, HomeChatViewModel homeChatViewModel, String fullName) {
        ConnectycubeStorage.uploadFileTask(file, true, null).performAsync(new EntityCallback<ConnectycubeFile>() {
            @Override
            public void onSuccess(ConnectycubeFile storageFile, Bundle bundle) {
                ConnectycubeAttachment attachment = new ConnectycubeAttachment("image/jpg");
                attachment.setId(storageFile.getId().toString());
                attachment.setUrl(storageFile.getPublicUrl());
                attachment.setName(fullName + "からの画像");
                homeChatViewModel.setAttachMess(attachment);
                homeChatViewModel.getSendChatMessAttachment().setValue(true);
            }
            @Override
            public void onError(ResponseException e) {
            }
        });
    }

    public static String getPathFromURI(Uri contentUri, Context context) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    // Method to save an bitmap to a file
    public static File bitmapToFile(Bitmap bitmap, Context context) {
        // Get the context wrapper

        // Initialize a new file instance to save bitmap object
        File file = context.getDir("Images", Context.MODE_PRIVATE);
        file = new File(file, "upload.jpg");

        try {
            // Compress the bitmap and save in jpg format
            OutputStream stream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            stream.flush();
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Return the saved bitmap uri
        return file;
    }


}
