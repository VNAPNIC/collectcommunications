package co.jp.collectcommunication.util

import android.app.ActivityManager
import android.app.ActivityManager.RunningAppProcessInfo
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager


object Utils {

    fun isApplicationSentToBackground(): Boolean {
        val myProcess = RunningAppProcessInfo()
        ActivityManager.getMyMemoryState(myProcess)
        return myProcess.importance != RunningAppProcessInfo.IMPORTANCE_FOREGROUND
    }

    fun showInputMethod(view: View): Boolean {
        val imm = view.context.getSystemService(
            Context.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        return imm?.showSoftInput(view, 0) ?: false
    }

    fun hideInputMethod(view: View): Boolean {
        val imm = view.context.getSystemService(
            Context.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        return imm?.hideSoftInputFromWindow(view.windowToken, 0) ?: false
    }
}