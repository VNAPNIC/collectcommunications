package co.jp.collectcommunication.util

import android.content.Context
import android.widget.ImageView
import co.jp.collectcommunication.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView

fun loadChatMessagePhoto(
    isPrivate: Boolean,
    url: String? = "",
    imageView: ImageView,
    ctx: Context
) {
    val placeholder =
        if (isPrivate) R.drawable.ic_avatar_placeholder else R.drawable.ic_avatar_placeholder_group

    Glide.with(ctx)
        .load(url)
        .placeholder(placeholder)
        .error(placeholder)
        .apply(RequestOptions.circleCropTransform())
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(imageView)
}

fun loadAttachImage(url: String?, imageView: ImageView, ctx: Context) {
    val placeholder = R.drawable.ic_image_black_24dp
    val width = ctx.resources.getDimensionPixelSize(R.dimen.attach_image_width)
    val height = ctx.resources.getDimensionPixelSize(R.dimen.attach_image_height)
    val requestOptions = RequestOptions().transform(RoundedCorners(16))
    Glide.with(ctx)
        .load(url)
        .placeholder(placeholder)
        .override(width, height)
        .dontTransform()
        .apply(requestOptions)
        .error(placeholder)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(imageView)
}

fun loadAvatarPhoto(
    url: String? = "",
    imageView: CircleImageView,
    ctx: Context
) {
    Glide.with(ctx)
        .load(url)
        .placeholder(R.drawable.icon_nophoto)
        .error(R.drawable.icon_nophoto)
        .apply(RequestOptions.circleCropTransform())
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(imageView)
}