package co.jp.collectcommunication

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.connectycube.chat.ConnectycubeRestChatService
import com.connectycube.chat.model.ConnectycubeAttachment
import com.connectycube.chat.model.ConnectycubeChatDialog
import com.connectycube.chat.model.ConnectycubeChatMessage
import com.connectycube.chat.model.ConnectycubeDialogType
import com.connectycube.chat.request.MessageGetBuilder
import com.connectycube.core.EntityCallback
import com.connectycube.core.exception.ResponseException
import com.connectycube.users.ConnectycubeUsers
import com.connectycube.users.model.ConnectycubeUser
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class HomeChatViewModel : ViewModel() {

    lateinit var shopInfo: ConnectycubeUser
    var chatDialog: MutableLiveData<ConnectycubeChatDialog>? = null
    var chatHistory: MutableLiveData<Boolean>? = null
    var messHistoryList: ArrayList<ConnectycubeChatMessage>? = arrayListOf()
    var checkContain: ArrayList<String> = arrayListOf()
    var items: ArrayList<Attachment> = arrayListOf()
    var dialogId: String = ""

    private var attachMess = ConnectycubeAttachment(ConnectycubeAttachment.IMAGE_TYPE)
    fun getAttachMess(): ConnectycubeAttachment {
        return attachMess
    }

    fun setAttachMess(data: ConnectycubeAttachment) {
        attachMess = data
    }

    var sendChatMessAttachment: MutableLiveData<Boolean>? = null
    fun getChatMessAttachmentStatus(): MutableLiveData<Boolean> {
        if (sendChatMessAttachment == null) sendChatMessAttachment = MutableLiveData()
        return sendChatMessAttachment!!
    }

    fun getDialogChat(): MutableLiveData<ConnectycubeChatDialog> {
        if (chatDialog == null) chatDialog = MutableLiveData()
        return chatDialog!!
    }

    fun getChatHistoryStatus(): MutableLiveData<Boolean> {
        if (chatHistory == null) chatHistory = MutableLiveData()
        return chatHistory!!
    }

    fun getUserShopInfo(context: Context) {
        ConnectycubeUsers.getUserByLogin(shopInfo.customData)
            .performAsync(object : EntityCallback<ConnectycubeUser> {
                override fun onSuccess(user: ConnectycubeUser, args: Bundle) {
                    Log.i("HomeChatViewModel",user.toString())
                    user.apply {
                        shopInfo = user
                        initChatDialog(user.id)
                    }
                }

                override fun onError(error: ResponseException) {
                    Toast.makeText(
                        context,
                        "サーバをメンテナンス中ですので、お待ちください。",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    fun initChatDialog(shopId: Int) {
        val occupantIds = ArrayList<Int>()
        occupantIds.add(shopId)
        val dialog = ConnectycubeChatDialog()
        dialog.type = ConnectycubeDialogType.PRIVATE
        dialog.setOccupantsIds(occupantIds)
        ConnectycubeRestChatService.createChatDialog(dialog)
            .performAsync(object : EntityCallback<ConnectycubeChatDialog> {
                override fun onSuccess(createdDialog: ConnectycubeChatDialog, params: Bundle) {
                    chatDialog?.value = createdDialog
                    this@HomeChatViewModel.dialogId = createdDialog.dialogId
                    getChatHistory(this@HomeChatViewModel.dialogId, 24)
                }

                override fun onError(exception: ResponseException) {

                }
            })
    }

    var timeTracker: Long? = null
    fun getChatHistory(dialogId: String, limit: Int) {
        val messageGetBuilder = MessageGetBuilder()
        messageGetBuilder.limit = limit
        messageGetBuilder.sortDesc("date_sent")
        messageGetBuilder.markAsRead(false)
        ConnectycubeRestChatService.getDialogMessages(
            ConnectycubeChatDialog(dialogId),
            messageGetBuilder
        )
            .performAsync(object : EntityCallback<ArrayList<ConnectycubeChatMessage>> {
                override fun onSuccess(
                    messages: ArrayList<ConnectycubeChatMessage>,
                    bundle: Bundle
                ) {
                    if (messages.size == 0) getChatHistoryStatus().value = true
                    messages.apply {
                        if (this.size == 0) return
                        timeTracker = messages[messages.size - 1].dateSent
                        messHistoryList?.addAll(messages)
                        getChatHistoryStatus().value = true
                    }
                }

                override fun onError(error: ResponseException) {
                    getChatHistoryStatus().value = true
                }
            })

    }


    fun getChatHistory(callback: EntityCallback<ArrayList<ConnectycubeChatMessage>>) {
        val messageGetBuilder = MessageGetBuilder()
        messageGetBuilder.sortDesc("date_sent")
        ConnectycubeRestChatService.getDialogMessages(
            ConnectycubeChatDialog(this@HomeChatViewModel.dialogId),
            messageGetBuilder
        ).performAsync(callback)
    }

    fun getAttachment(messages: ArrayList<ConnectycubeChatMessage>) {
        items.clear()
        checkContain.clear()
        for (item in messages) {
            if (withAttachment(item)) {
                val date = formatDate(item.dateSent)
                if (!checkContain.contains(date)) {
                    checkContain.add(date)
                    items.add(Attachment(AttachmentType.TITLE, date, null))

                    item.attachments.forEach {
                        items.add(Attachment(AttachmentType.ITEM, date, it))
                    }
                } else {
                    item.attachments.forEach {
                        items.add(Attachment(AttachmentType.ITEM, date, it))
                    }
                }
            }
        }
    }

    fun loadMore() {
        val chatDialog = ConnectycubeChatDialog(chatDialog?.value?.dialogId)

        val messageGetBuilder = MessageGetBuilder()
        messageGetBuilder.limit = 24
        messageGetBuilder.sortDesc("date_sent")
        messageGetBuilder.markAsRead(false)
        messageGetBuilder.lt("date_sent", timeTracker)

        ConnectycubeRestChatService.getDialogMessages(chatDialog, messageGetBuilder)
            .performAsync(object : EntityCallback<ArrayList<ConnectycubeChatMessage>> {
                override fun onSuccess(
                    messages: ArrayList<ConnectycubeChatMessage>,
                    bundle: Bundle
                ) {
                    if (messages.size == 0) getChatHistoryStatus().value = true
//                    for (item in messages) {
//                        if (withAttachment(item)) {
//
//                            val date = formatDate(item.dateSent)
//                            if (!checkContain.contains(date)) {
//                                checkContain.add(date)
//                                items.add(Attachment(AttachmentType.TITLE,date,null))
//                                item.attachments.forEach {
//                                    items.add(Attachment(AttachmentType.ITEM,date,it))
//                                }
//                            } else {
//                                item.attachments.forEach {
//                                    items.add(Attachment(AttachmentType.ITEM,date,it))
//                                }
//                            }
//                        }
//                    }
                    messages.apply {
                        if (this.size == 0) return
                        timeTracker = messages[this.size - 1].dateSent
                        messHistoryList?.addAll(messages)
                    }
                    getChatHistoryStatus().value = true
                }

                override fun onError(error: ResponseException) {
                    getChatHistoryStatus().value = true
                }
            })

    }

    fun withAttachment(chatMessage: ConnectycubeChatMessage): Boolean {
        return !chatMessage.attachments.isNullOrEmpty()
    }


    fun formatDate(seconds: Long): String {
        val date = Date(seconds * 1000)
        val df2 = SimpleDateFormat("yyyy-MM-dd")
        return df2.format(date)
    }
}