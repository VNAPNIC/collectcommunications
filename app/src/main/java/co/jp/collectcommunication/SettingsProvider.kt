package co.jp.collectcommunication

import android.content.Context
import co.jp.collectcommunication.beans.ApplicationBean
import com.connectycube.auth.session.ConnectycubeSettings
import com.connectycube.chat.ConnectycubeChatService
import com.connectycube.chat.connections.tcp.TcpChatConnectionFabric
import com.connectycube.chat.connections.tcp.TcpConfigurationBuilder
import com.connectycube.core.LogLevel
import logLevel

object SettingsProvider {

    // long@oluxe.co.jp
    // batigol123
    // internal val APP_ID = "909"
    // internal val AUTH_KEY = "HOdOPkWQ-KVb9Om"
    // internal val AUTH_SECRET = "B25c38vbvFyQhbE"
    // internal val ACCOUNT_KEY = "v3wWF3uEsmXDk7pj-ySk"

    // oluxe_dev@oluxe.co.jp
    // oluxe123
    // internal val APP_ID = "1192"
    // internal val AUTH_KEY = "eDARv4RNVR6HXzt"
    // internal val AUTH_SECRET = "WnJfHPXKZxLZT9O"
    // internal val ACCOUNT_KEY = "bh_byYBvVJrSP3J4RT_a"

    // laideplus@collectyou.jp
    // laide123
    // private val APP_ID = "1279"
    // private val AUTH_KEY = "4xHuf8N3HB55Yhm"
    // private val AUTH_SECRET = "gXky7Z7SBTgMamA"
    // private val ACCOUNT_KEY = "qz69qpazXqtWxry-htTa"

    fun initConnectycubeCredentials(
        applicationContext: Context,
        bean: ApplicationBean
    ) {

        print("ConnectycubeSettings " +
                "\n LogLevel: $logLevel" +
                "\n APPLICATION_ID: ${bean.chat_id}" +
                "\n AUTHORIZATION KEY: ${bean.chat_authkey}" +
                "\n AUTHORIZATION SECRET: ${bean.chat_authsecret}" +
                "\n ACCOUNT KEY: ${bean.chat_accountkey}\n")

        ConnectycubeSettings.getInstance().logLevel = logLevel
        ConnectycubeSettings.getInstance().init(
            applicationContext,
            bean.chat_id.toString(),
            bean.chat_authkey,
            bean.chat_authsecret
        )
        ConnectycubeSettings.getInstance().accountKey = bean.chat_accountkey
    }

    fun initChat() {
        ConnectycubeSettings.getInstance().logLevel = LogLevel.DEBUG
        ConnectycubeChatService.setDebugEnabled(true)
        ConnectycubeChatService.setDefaultConnectionTimeout(30000)
        ConnectycubeChatService.getInstance().setUseStreamManagement(true)

        val builder = TcpConfigurationBuilder()
            .setAllowListenNetwork(true)
            .setUseStreamManagement(true)
        ConnectycubeChatService.setConnectionFabric(TcpChatConnectionFabric(builder.apply {
            socketTimeout = 0
        }))
    }
}